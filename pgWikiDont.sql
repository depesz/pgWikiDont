-- Make temporary table so other elements can store their data
\ir parts/_initialize.sql

-- Run all the checks
\ir parts/sqlascii.sql
\ir parts/rules.sql
\ir parts/inheritance.sql
\ir parts/not-in.sql
\ir parts/uppercase.sql
\ir parts/between.sql
\ir parts/timestamp.sql
\ir parts/timetz.sql
\ir parts/current_time.sql
\ir parts/timestamp0.sql
\ir parts/char.sql
\ir parts/varchar.sql
\ir parts/money.sql
\ir parts/serial.sql
\ir parts/trust.sql

-- Show report and drop temp table
\ir parts/_finalize.sql
