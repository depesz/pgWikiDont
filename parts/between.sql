INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( distinct query order by query ) as a
    FROM :"ttqueries"
    WHERE query ~ ' between '
)
select 'Don''t use BETWEEN (especially with timestamps)', 'Don.27t_use_BETWEEN_.28especially_with_timestamps.29', format('You have %s query/queries that use BETWEEN clause:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
