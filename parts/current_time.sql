INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( distinct query order by query ) as a
    FROM :"ttqueries"
    WHERE query ~ '\mcurrent_time\M'
)
select 'Don''t use CURRENT_TIME', 'Don.27t_use_CURRENT_TIME', format('You have %s query/queries that use CURRENT_TIME clause:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
