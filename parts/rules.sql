INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    select array_agg( format('%s on %s.%s', rulename, schemaname, tablename) order by schemaname, tablename, rulename) as a from pg_rules  where schemaname <> 'pg_catalog'
)
select 'Don''t use rules', 'Don.27t_use_rules', format('You have %s rule(s) in this database:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
